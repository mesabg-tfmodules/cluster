locals {
  ecs_optimized_ami = jsondecode(data.aws_ssm_parameter.ecs_optimized_ami.insecure_value)["image_id"]
}
