output "cluster_id" {
  value       = aws_ecs_cluster.ecs_cluster.id
  description = "Created ECS cluster identifier"
}

output "cluster_arn" {
  value       = aws_ecs_cluster.ecs_cluster.arn
  description = "Created ECS cluster ARN"
}
