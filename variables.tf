variable "environment" {
  type = string
  description = "Current environment"
}

variable "name" {
  type        = string
  description = "General cluster name"
}

variable "instance" {
  type = object({
    type          = string
    profile       = string
    ebs_optimized = bool
    key_name      = string
    volume_size   = number
  })
  description     = "Instances description for cluster"
}

variable "cluster" {
  type = object({
    capacity_utilization_percentage = number  # When capacity of EC2 instances is at this percentage, an autoscaling event will be triggered
    minimum_scaling_step_size       = number  # Minimum step for an autoscaling event
    maximum_scaling_step_size       = number  # Maximum step for an autoscaling event
    min_healthy_percentage          = number  # Minimum percentage of healthy instances to retain active on a service deployment (blue/green rollout)
    max_instance_lifetime           = number  # Maximum amount of time (seconds) before refresh an instance
    autoscaling_group_min_size      = number  # Minimum amount of instances allowed in the autoscaling group
    autoscaling_group_max_size      = number  # Maximum amount of instances allowed in the autoscaling group
  })
  description = "Cluster configuration"
  default     = {
    capacity_utilization_percentage = 100
    minimum_scaling_step_size       = 1
    maximum_scaling_step_size       = 1000
    min_healthy_percentage          = 50
    max_instance_lifetime           = 691200 # 8 days of lifetime
    autoscaling_group_min_size      = 0
    autoscaling_group_max_size      = 100
  }
}

variable "network" {
  type = object({
    subnet_ids      = list(string)
    security_groups = list(string)
  })
  description = "Network identifiers"
}

variable "logs_enabled" {
  type        = bool
  description = "Enable or disable logs monitoring"
  default     = true
}
