resource "aws_launch_configuration" "ecs_launch_configuration" {
  name_prefix           = var.name
  image_id              = local.ecs_optimized_ami
  instance_type         = var.instance.type

  iam_instance_profile  = var.instance.profile
  enable_monitoring     = var.logs_enabled
  ebs_optimized         = var.instance.ebs_optimized
  key_name              = var.instance.key_name

  user_data             = <<CONFIGURATION
  #!/bin/bash

  # Cluster configuration
  echo ECS_CLUSTER=${var.name} >> /etc/ecs/ecs.config;
  echo ECS_LOGLEVEL=debug >> /etc/ecs/ecs.config;
  echo ECS_WARM_POOLS_CHECK=true >> /etc/ecs/ecs.config;

  # Restart cluster service
  stop ecs
  start ecs
  CONFIGURATION

  root_block_device {
    volume_type           = "standard"
    volume_size           = var.instance.volume_size
    delete_on_termination = true
  }

  security_groups         = var.network.security_groups

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "autoscaling_group" {
  name                      = var.name
  vpc_zone_identifier       = var.network.subnet_ids
  launch_configuration      = aws_launch_configuration.ecs_launch_configuration.name

  min_size                  = var.cluster.autoscaling_group_min_size
  max_size                  = var.cluster.autoscaling_group_max_size
  health_check_grace_period = 300
  health_check_type         = "EC2"
  protect_from_scale_in     = false
  capacity_rebalance        = true
  max_instance_lifetime     = var.cluster.max_instance_lifetime
  metrics_granularity       = var.logs_enabled ? "1Minute" : null
  enabled_metrics           = var.logs_enabled ? [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
    "GroupInServiceCapacity",
    "GroupPendingCapacity",
    "GroupStandbyCapacity",
    "GroupTerminatingCapacity",
    "GroupTotalCapacity",
  ] : null

  # Every launch configuration template change triggers a refresh event
  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = var.cluster.min_healthy_percentage
    }
  }

  warm_pool {
    min_size                    = 1
    max_group_prepared_capacity = 2

    instance_reuse_policy {
      reuse_on_scale_in         = true
    }
  }

  tag {
    key                 = "AmazonECSManaged"
    value               = true
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = var.name
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = var.environment
    propagate_at_launch = true
  }
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name                = var.name
  
  dynamic "setting" {
    for_each = var.logs_enabled == true ? [1] : []
    content {
      name  = "containerInsights"
      value = "enabled"
    }
  }

  tags = {
    Name              = var.name
    Environment       = var.environment
  }
}

resource "aws_ecs_capacity_provider" "ecs_capacity_provider" {
  name  = var.name

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.autoscaling_group.arn
    managed_draining               = "ENABLED"
    managed_termination_protection = "DISABLED"

    managed_scaling {
      maximum_scaling_step_size = var.cluster.maximum_scaling_step_size
      minimum_scaling_step_size = var.cluster.minimum_scaling_step_size
      status                    = "ENABLED"
      target_capacity           = var.cluster.capacity_utilization_percentage
    }
  }
}

resource "aws_ecs_cluster_capacity_providers" "cluster_capacity_providers" {
  cluster_name = aws_ecs_cluster.ecs_cluster.name

  capacity_providers  = [aws_ecs_capacity_provider.ecs_capacity_provider.name]

  default_capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
  }
}
