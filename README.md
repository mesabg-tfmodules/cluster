# Cluster Module

This module is capable to generate a fully functional configurated ECS cluster.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general name
- `instance` - instance configurations
- `cluster` - cluster configurations (default to {desired_capacity: 1, min_size: 1, max_size: 2})
- `network` - network configurations
- `logs_enabled` - enable/disable cloudwatch logs (default true)
- `efs` - rexray/efs configuration (default null)


Usage
-----

```hcl
module "cluster" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/cluster.git"

  environment         = "environment"
  name = {
    slug              = "slugname"
    full              = "Full Name"
  }

  logs_enabled        = true

  instance = {
    type              = "t2.micro"
    ami               = "ami-rrrrr"
    profile           = "arn:aws:iam::instance-profile"
    ebs_optimized     = true
    key_name          = "somekey"
    volume_size       = 30
  }

  cluster = {
    desired_capacity  = 1
    min_size          = 1
    max_size          = 1
  }

  network = {
    subnet_ids        = ["subnet-xxxx", "subnet-aaaa"]
    security_groups   = ["subnet-xxxx", "subnet-aaaa"]
  }

  efs = {
    access_key        = "some"
    secret_key        = "some"
    region            = "some"
    security_groups   = "some"
  }
}
```

Outputs
=======

 - `cluster_id` - Created ECS cluster identifier
 - `cluster_arn` - Created ECS cluster arn


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
